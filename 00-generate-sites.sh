# Generate empty Hugo sites
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'cd ..; hugo new site %  > /dev/null 2>&1;  cd %; git init; git add .; git commit -m "Initial commit"'
# Load the theme
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'cd ../%; wget https://gitlab.com/zadna-schools/zadna-theme-national/-/archive/main/zadna-theme-national-main.tar.gz; tar xf zadna-theme-national-main.tar.gz; rm -rf zadna-theme-national-main.tar.gz; mv zadna-theme-national-main themes/zadna-theme-national; \cp -av themes/zadna-theme-national/exampleSite/* .;  \cp -av themes/zadna-theme-national/exampleSite/.* .; rm -f config.toml; rm -rf themes/zadna-theme-national/exampleSite; git add .; git commit -m "Add theme"'

# Configure README with site-specific details
awk -F "|" '{ print "# "$1"\nWebsite for "$3 > "../"$1"/README.md"}' zaschools.csv

# Configure config.toml with site-specific details
awk -F "|" '{ print "baseURL = \"/""\"\ntitle = \""$3"\"\ncopyright = \"2022\"\n\nthemesDir = \"themes\"\ntheme = \"zadna-theme-national\"\n\nlanguageCode = \"en-za\"" > "../"$1"/config/_default/config.toml"}' zaschools.csv

# Configure user parameters in params.toml
awk -F "|" '{ print "# Configure school-specific data\ndescription = \""$4"\"\nemisNo = \""$5"\"\nphysicalAddress = \""$6"\"\ntown = \""$7"\"\nprovince = \""$8"\"\nphone = \""$9"\"\nemail = \""$10"\"\nprincipal = \""$11"\"\n" > "../"$1"/config/_default/params.toml"}' zaschools.csv

# Configure Google Analytics and Map API
awk -F "|" '{ print "# Configure Google Analytics\ngoogle_analytics_id = \"\"\nenableGoogleMaps = \"true\"\n\n# Configure Google Map API key\ngoogle_tag_manager_id = \"\"\ngoogleMapsAPIKey = \"AIzaSyDtgKrJGP75UGAY0LIFRTn7ah4WKmFGek8\"\n\nlatitude = \""$12"\"\nlongitude = \""$13"\"" >> "../"$1"/config/_default/params.toml"}' zaschools.csv

# Configure Home
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'echo -e "\n[homepage]\n  show_contact_box = true\n  show_services_button = true" >> ../%/config/_default/params.toml;'
# Configure [logo]
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'echo -e "\n[logo]\n  mobile = \"images/logo/dbe-logo.png\"\n  mobile_height = \"36px\"\n  desktop = \"images/logo/dbe-logo.png\"\n  desktop_height = \"55px\"\n  alt = \"Department of Basic Education Logo\"" >> ../%/config/_default/params.toml;'
# Configure [fonts]
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'echo -e "\n[fonts]\n  google_fonts = \"https://fonts.googleapis.com/css2?family=Playfair+Display:wght@600&family=Source+Sans+Pro:wght@400;700&display=swap\"\n  heading = \"Playfair Display\"\n  base = \"Source Sans Pro\"" >> ../%/config/_default/params.toml;'
# Configure [colors]
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'echo -e "\n[colors]\n  primary = \"#bf952a\"\n  black = \"#2f2f41\"\n  white = \"#ffffff\"\n  white_offset = \"#f6f7ff\"\n  grey = \"#5C5A5A\"" >> ../%/config/_default/params.toml;'
# Configure [team]
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'echo -e "\n[team]\n  summary_large_truncate = 120" >> ../%/config/_default/params.toml;'
# Configure [theme]
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'echo -e "\n[theme]\n  theme_text = \047Theme by <a href=\"https://www.archton.io\">www.archton.io</a>\047" >> ../%/config/_default/params.toml;'
# Push changes
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'cd ../%; git add .; git commit -m "Configure parameters"'

# Generate the deployment folders
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'mkdir ../%-app; mkdir ../%-app/prod;'
# Configure README with site-specific details
awk -F "|" '{ print "# "$1"\nArgoCD YAML files for the deployment of "$1" ("$3") to Kubernetes at nodePort "$2"." > "../"$1"-app/README.md"}' zaschools.csv
# Generate YAML files
go run generate-deployments/generate-deployments.go
# Commit changes
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'cd ../%-app; git init; git add .; git commit -m "Configure deployment"'
