# Create the GitLab repos for the deployments first
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c "curl --request POST --header \"PRIVATE-TOKEN: glpat-767qL7ksJm8tx6i9ivSj\" --header \"Content-Type: application/json\" --data '{\"name\": \"%-app\", \"namespace_id\": \"56828212\", \"visibility\": \"public\"}' --url https://gitlab.com/api/v4/projects/"
# Push the sites to the repos
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'cd ../%-app; git remote add origin git@gitlab.com:zadna-schools/%-app.git; git push -u origin --all'

# Create the GitLab repos for the website content
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c "curl --request POST --header \"PRIVATE-TOKEN: glpat-767qL7ksJm8tx6i9ivSj\" --header \"Content-Type: application/json\" --data '{\"name\": \"%\", \"namespace_id\": \"56828212\", \"visibility\": \"public\"}' --url https://gitlab.com/api/v4/projects/"
# Push the sites to the repos
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'cd ../%; git remote add origin git@gitlab.com:zadna-schools/%.git; git push -u origin --all'
