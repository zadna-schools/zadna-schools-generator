# Websites at scale

The project is driven by the spreadsheet:
- Export as CSV
- Use a pipe delimiter, 
- Don't quote text cells
- Delete the header
- Replace non-ASCII characters to keep folder names sane

Execute from this directory:
```
./00-generate-sites.sh
./01-create-repos.sh
./02-configure-apps.sh
./03-deploy-apps.sh
./04-update-apps.sh
```

Purge all the sites and their repos:
```
./99-purge-sites.sh
```

Each time the content of a website is updated, rerun this:
```
./04-update-apps.sh
```
