package main

import (
	"fmt"
	"log"

	"github.com/xanzy/go-gitlab"
)

func getProjects() []int {
	var projectList []int

	git, err := gitlab.NewClient("glpat-767qL7ksJm8tx6i9ivSj")
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	projects, _, err := git.Projects.ListProjects(&gitlab.ListProjectsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
		Owned:  gitlab.Bool(true),
		Search: gitlab.String("zaschool"),
	})
	if err != nil {
		log.Fatalf("Failed to list Projects: %v", err)
	}

	for _, project := range projects {
		fmt.Println(project.ID, project.Name)
		projectList = append(projectList, project.ID)
	}
	return projectList
}

func deleteProject(pid int) {

	git, err := gitlab.NewClient("glpat-767qL7ksJm8tx6i9ivSj")
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	_, other_err := git.Projects.DeleteProject(pid)
	if other_err != nil {
		log.Fatalf("Failed to delete Project: %v", other_err)
	}
}

func getRegistries(pid int) []int {
	var registryList []int

	git, err := gitlab.NewClient("glpat-767qL7ksJm8tx6i9ivSj")
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	registries, _, err := git.ContainerRegistry.ListProjectRegistryRepositories(pid, &gitlab.ListRegistryRepositoriesOptions{})
	if err != nil {
		log.Fatalf("Failed to list ProjectRegistryRepositories: %v", err)
	}

	for _, registry := range registries {
		fmt.Println(registry.ID, registry.Location)
		registryList = append(registryList, registry.ID)
	}
	return registryList
}

func deleteRegistry(pid int, reg int) {

	git, err := gitlab.NewClient("glpat-767qL7ksJm8tx6i9ivSj")
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	_, other_err := git.ContainerRegistry.DeleteRegistryRepository(pid, reg)
	if other_err != nil {
		log.Fatalf("Failed to delete RegistryRepository: %v", other_err)
	}
}

func main() {
	projectList := getProjects()
	for _, projectID := range projectList {
		registryList := getRegistries(projectID)
		for _, registryID := range registryList {
			deleteRegistry(projectID, registryID)
			fmt.Printf("Deleted ContainerRegistry %v for Project %v\n", registryID, projectID)
		}
	}
	for _, projectID := range projectList {
		deleteProject(projectID)
		fmt.Printf("Deleted Project %v\n", projectID)
	}

}
