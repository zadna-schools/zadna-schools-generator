package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

// Read a whole file into the memory and store it as array of lines
func readLines(path string) (lines []string, err error) {
	var (
		file   *os.File
		part   []byte
		prefix bool
	)

	if file, err = os.Open(path); err != nil {
		return
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	buffer := bytes.NewBuffer(make([]byte, 0))
	for {
		if part, prefix, err = reader.ReadLine(); err != nil {
			break
		}
		buffer.Write(part)
		if !prefix {
			lines = append(lines, buffer.String())
			buffer.Reset()
		}
	}
	if err == io.EOF {
		err = nil
	}
	return
}

var (
	newFile *os.File
	err     error
)

func generateApplications() {
	lines, err := readLines("zaschools.csv")
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return
	}
	for _, line := range lines {
		var siteDetails = strings.Split(line, "|")
		fmt.Printf("Create application files for %v with port %v\n", siteDetails[0], siteDetails[1])

		newFile, err = os.Create("../" + siteDetails[0] + "-app/application.yaml")
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Create: ", newFile.Name())
		newFile.Close()

		file, err := os.OpenFile(
			"../"+siteDetails[0]+"-app/application.yaml",
			os.O_WRONLY|os.O_TRUNC|os.O_CREATE,
			0666,
		)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		// Write bytes to file
		byteSlice := []byte(
			"apiVersion: argoproj.io/v1alpha1\n" +
				"kind: Application\n" +
				"metadata:\n" +
				"  name: " + siteDetails[0] + "-app\n" +
				"  namespace: argocd\n" +
				"spec:\n" +
				"  project: default\n" +
				"  source:\n" +
				"    repoURL: https://gitlab.com/zadna-schools/" + siteDetails[0] + "-app.git\n" +
				"    targetRevision: HEAD\n" +
				"    path: prod\n" +
				"  destination:\n" +
				"    server: https://kubernetes.default.svc\n" +
				"    namespace: zaschools\n" +
				"  syncPolicy:\n" +
				"    syncOptions:\n" +
				"    - CreateNamespace=true\n" +
				"    automated:\n" +
				"      selfHeal: true\n" +
				"      prune: true\n")

		bytesWritten, err := file.Write(byteSlice)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Wrote %d bytes to "+siteDetails[0]+"-app/application.yaml\n", bytesWritten)
	}
}

func generateDeployments() {
	lines, err := readLines("zaschools.csv")
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return
	}
	for _, line := range lines {
		var siteDetails = strings.Split(line, "|")
		fmt.Printf("Create deployment files for %v\n", siteDetails[0])

		newFile, err = os.Create("../" + siteDetails[0] + "-app/prod/deployment.yaml")
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Create: ", newFile.Name())
		newFile.Close()

		file, err := os.OpenFile(
			"../"+siteDetails[0]+"-app/prod/deployment.yaml",
			os.O_WRONLY|os.O_TRUNC|os.O_CREATE,
			0666,
		)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		// Write bytes to file
		byteSlice := []byte(
			"apiVersion: apps/v1\n" +
				"kind: Deployment\n" +
				"metadata:\n" +
				"  name: " + siteDetails[0] + "\n" +
				"  namespace: zaschools\n" +
				"spec:\n" +
				"  selector:\n" +
				"    matchLabels:\n" +
				"      app: " + siteDetails[0] + "\n" +
				"  replicas: 1\n" +
				"  template:\n" +
				"    metadata:\n" +
				"      labels:\n" +
				"        app: " + siteDetails[0] + "\n" +
				"    spec:\n" +
				"      containers:\n" +
				"      - name: " + siteDetails[0] + "\n" +
				"        image: registry.gitlab.com/zadna-schools/" + siteDetails[0] + ":latest\n" +
				"        resources:\n" +
				"          requests:\n" +
				"            memory: '64Mi'\n" +
				"            cpu: '250m'\n" +
				"          limits:\n" +
				"            memory: '128Mi'\n" +
				"            cpu: '500m'\n" +
				"        ports:\n" +
				"        - containerPort: 80")

		bytesWritten, err := file.Write(byteSlice)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Wrote %d bytes to "+siteDetails[0]+"-app/prod/deployment.yaml\n", bytesWritten)
	}
}

func generateServices() {
	lines, err := readLines("zaschools.csv")
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return
	}
	for _, line := range lines {
		var siteDetails = strings.Split(line, "|")
		fmt.Printf("Create service file for %v\n", siteDetails[0])

		newFile, err = os.Create("../" + siteDetails[0] + "-app/prod/service.yaml")
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Create: ", newFile.Name())
		newFile.Close()

		file, err := os.OpenFile(
			"../"+siteDetails[0]+"-app/prod/service.yaml",
			os.O_WRONLY|os.O_TRUNC|os.O_CREATE,
			0666,
		)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		// Write bytes to file
		byteSlice := []byte(
			"apiVersion: v1\n" +
				"kind: Service\n" +
				"metadata:\n" +
				"  name: " + siteDetails[0] + "-svc\n" +
				"  namespace: zaschools\n" +
				"spec:\n" +
				"  type: NodePort\n" +
				"  selector:\n" +
				"    app: " + siteDetails[0] + "\n" +
				"  ports:\n" +
				"  - nodePort: " + siteDetails[1] + "\n" +
				"    port: 80\n" +
				"    targetPort: 80\n")
		bytesWritten, err := file.Write(byteSlice)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Wrote %d bytes to "+siteDetails[0]+"-app/prod/service.yaml\n", bytesWritten)
	}
}

func main() {
	generateApplications()
	generateDeployments()
	generateServices()
}
