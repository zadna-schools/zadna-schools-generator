# Delete applications
ls ../*-app/application.yaml | xargs -t -I % sh -c 'kubectl delete -f %'
# Delete namespace
kubectl delete ns zaschool
