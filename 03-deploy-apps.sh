# Deploy the apps
awk -F "|" '{print $1}' zaschools.csv | xargs -t -I % sh -c 'cd ../%-app; kubectl apply -f application.yaml'
